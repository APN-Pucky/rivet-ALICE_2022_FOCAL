BEGIN PLOT /ALICE_2022_FOCAL/.*
LegendAlign=l
PlotSize=10,10
LegendXPos=0.00
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/pT
XLabel=$p_{T,\gamma}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\gamma}$ [pb / GeV]
LegendAlign=l
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/pTPhoton_frac
XLabel=$p_{T,\gamma}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\gamma}$ [pb / GeV]
LegendAlign=l
LogY=0
LogX=0
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/DeltaPhi
XLabel=$\Delta\Phi$ [rad]
YLabel=$\text{d}\sigma / \text{d}\Delta\Phi $ [pb / rad]
RatioPlot=0
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/DeltaRap
XLabel=$\Delta\eta$
YLabel=$\text{d}\sigma / \text{d}\Delta\eta $ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/dPhidRap
XLabel=$\Delta\phi$
YLabel=$\Delta\eta$
ZLabel=$\text{d}^2\sigma / \text{d}\Delta\eta\text{d}\Delta\phi$ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/Rap
XLabel=$\eta$ 
YLabel=$\text{d}\sigma / \text{d}\eta $ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/xJ
XLabel=$x_J$
YLabel=$\text{d}\sigma / \text{d}x_J $ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/Q
XLabel=$Q$ [GeV]
YLabel=$\text{d}\sigma / \text{d}Q $ [pb/GeV]
END PLOT



BEGIN PLOT /ALICE_2022_FOCAL/xg
XLabel=$x_g$ 
YLabel=$\text{d}\sigma / \text{d}x_g $ [pb]
LogX=1
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/xg-Q
XLabel=$x_g$ 
YLabel=$Q$ [GeV]
ZLabel=$\text{d}^2\sigma / \text{d}x_g \text{d}Q $ [pb/GeV]
LogX=1
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/pTPhoton-Q
XLabel=$p_{T,\gamma}$ [GeV]
YLabel=$Q$ [GeV]
ZLabel=$\text{d}^2\sigma / \text{d}p_{T,\gamma} \text{d}Q $ [pb/GeV$^2$]
LogY=0
LogX=0
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/comptonrecoilyield
XLabel=$x_g$ 
YLabel=$Q$ [GeV]
ZLabel=$\text{d}^2N / \text{d}x_g \text{d}Q $ [pb/GeV]
LogX=1
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/dphi_deta
XLabel=$\Delta\phi$
YLabel=$\Delta\eta$
ZLabel=$\text{d}^2\sigma / \text{d}\Delta\eta\text{d}\Delta\phi$ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/dphi_drap
XLabel=$\Delta\phi$
YLabel=$\Delta Rap$
ZLabel=$\text{d}^2\sigma / \text{d}\Delta Rap \text{d}\Delta\phi$ [pb]
END PLOT

BEGIN PLOT /ALICE_2022_FOCAL/dR_jet
XLabel=$\Delta R$
YLabel=$E_{T,j}$ [GeV]
ZLabel=$\text{d}^2\sigma / \text{d}E_{T,j} \text{d}\Delta R$ [pb/GeV]
END PLOT



