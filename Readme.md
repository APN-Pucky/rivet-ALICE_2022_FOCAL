# Experimental Rivet Analysis for ALICE FOCAL prompt photons

```
$ make install
```
will build and install `RivetAnalysis.so` with its data to the directory found by `rivet-config`.

You can inspect exemplary results here: <https://apn-pucky.gitlab.io/rivet-ALICE_2022_FOCAL/>
