#ifndef _H_AB_HEADER_
#define _H_AB_HEADER_
/*
 * Some utility macros to keep code smaller.
 */

#define CONCAT(a, b) CONCAT_INNER(a, b)
#define CONCAT_INNER(a, b) a##b
#define UNIQUE_NAME(base) CONCAT(base, __COUNTER__)

#define IN(v, l, h) (l < v && v < h)
#define AUTO_IN_BIN(v, arr, i) IN(v, arr[i], arr[i + 1])

#define STR_VAR_VAL(var, val) "_" + #var + "-" + std::to_string(val)
#define STR_VAR(var) STR_VAR_VAL(var, var)

#define STR_BIN(arr, i) "_" + #arr + "-" + std::to_string(arr[i]) + "-" + std::to_string(arr[i + 1])

#define _AUTO_FOR_BIN_COND(cond, arr, i, r) \
	std::string r = region;                 \
	region = r + STR_BIN(arr, 0);           \
	for (size_t i = 0;                      \
		 i < arr.size() - 1;                \
		 ++i, region = r + STR_BIN(arr, i)) \
		if (!isAnalyse<s>() || cond)
#define AUTO_FOR_BIN_COND(cond, arr) _AUTO_FOR_BIN_COND(cond, arr, UNIQUE_NAME(AUTO), UNIQUE_NAME(AUTO))

#define _AUTO_FOR_BIN(val, arr, i, r) \
	_AUTO_FOR_BIN_COND(AUTO_IN_BIN(val, arr, i), arr, i, r)
#define AUTO_FOR_BIN(cond, arr) _AUTO_FOR_BIN(cond, arr, UNIQUE_NAME(AUTO), UNIQUE_NAME(AUTO))

// TODO loop over something not in split name (eg. num of jets)!

#define _AUTO_CONDITION(cond, i, r)                              \
	std::string r = region;                                      \
	region = r + STR_VAR_VAL(cond, 0);                           \
	for (size_t i = 0;                                           \
		 i < 2;                                                  \
		 ++i, region = (i < 2) ? (r + STR_VAR_VAL(cond, i)) : r) \
		if (!isAnalyse<s>() || cond == i)
#define AUTO_CONDITION(cond) _AUTO_CONDITION(cond, UNIQUE_NAME(AUTO), UNIQUE_NAME(AUTO))

#define _AUTO_CONDITION_DROP(cond, i, r)                         \
	std::string r = region;                                      \
	region = r;                                                  \
	for (size_t i = 0;                                           \
		 i < 2;                                                  \
		 ++i, region = (i < 2) ? (r + STR_VAR_VAL(cond, i)) : r) \
		if (!isAnalyse<s>() || cond == i || i == 0)
#define AUTO_CONDITION_DROP(cond) _AUTO_CONDITION_DROP(cond, UNIQUE_NAME(AUTO), UNIQUE_NAME(AUTO))

#define _AUTO_CONDITION_TRUE(cond, i, r) \
	std::string r = region;              \
	region = r + STR_VAR_VAL(cond, 1);   \
	for (size_t i = 1;                   \
		 i < 2;                          \
		 ++i, region = r)                \
		if (!isAnalyse<s>() || cond == i)
#define AUTO_CONDITION_TRUE(cond) _AUTO_CONDITION_TRUE(cond, UNIQUE_NAME(AUTO), UNIQUE_NAME(AUTO))

#define VALUE(var, i, r)    \
	std::string r = region; \
	region = r + STR_VAR(var);

#define AUTO_BOOK(_h, str, ...) \
	if (isInit<s>())            \
		book(_h[str + region], str + region, __VA_ARGS__);
#define AUTO_FILL(_h, str, ...) \
	if (isAnalyse<s>())         \
		_h[str + region]->fill(__VA_ARGS__);
#define AUTO_BOOK_FILL_1(str, b1, d1) \
	AUTO_BOOK(_h, str, b1)            \
	AUTO_FILL(_h, str, d1)
#define AUTO_BOOK_FILL AUTO_BOOK_FILL_1
#define AUTO_BOOK_FILL_2(str, b1, b2, d1, d2) \
	AUTO_BOOK(_h2, str, b1, b2)               \
	AUTO_FILL(_h2, str, d1, d2)
#define AUTO_FINALIZE() \
	if (isFinalize<s>())
#endif