// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ZFinder.hh"

#include "Rivet/Particle.hh"
#include "Rivet/Tools/Cuts.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Tools/ParticleUtils.hh"
#include "Rivet/Tools/RivetFastJet.hh"

/**
 * @brief
 * Comparable:
 *  - D0_2006_S6438750    Inclusive isolated photon cross-section, differential in pT(gamma)
 *  - ATLAS_2019_I1772071 Measurement of isolated-photon plus two-jet production
 *  - ATLAS_2017_I1645627 Isolated photon + jets at 13 TeV
 */
#include "ALICE_2022_FOCAL.h"

namespace Rivet
{
  enum Stage
  {
    INIT,
    ANALYSE,
    FINALIZE,
  };

  template <Stage s>
  bool inline isAnalyse()
  {
    return false;
  }
  template <>
  bool inline isAnalyse<Stage::ANALYSE>() { return true; }

  template <Stage s>
  bool inline isFinalize() { return false; }
  template <>
  bool inline isFinalize<Stage::FINALIZE>() { return true; }

  template <Stage s>
  bool inline isInit() { return false; }
  template <>
  bool inline isInit<Stage::INIT>() { return true; }

  /// @brief Add a short analysis description here
  class ALICE_2022_FOCAL : public Analysis
  {

  public:
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ALICE_2022_FOCAL);

    const double ISO_CONE_RADIUS = 0.4;
    // TODO check eta vs rapidity
    const double ISO_CONE_ET = 2.0;

    // maximum number of jets analyzed
    const unsigned int max_num_jets = 2;
    // TODO check fastrapidity fastjet what is it!
    const double JET_R = 0.4;

    const vector<double> ph_pt_bins = {5, 20, 40};
    const vector<double> ph_eta_bins = {3.4, 5.8};
    const vector<double> j_pt_bins = {20, 30, 60};
    const vector<double> j_eta_bins = {3.4, 5.8};

    const vector<double> xg_bins = logspace(10, 0.000001, 1.0);
    const vector<double> Q_bins = linspace(10, 0.000001, 30.0);

    const vector<double> rap_bins = linspace(10, 0., 7.);
    const vector<double> x_bins = linspace(4, 0., 2.);
    const vector<double> Q_bins_lin = linspace(50, 0., 30.0);
    const vector<double> dphi_bins = linspace(8, 0., M_PI);
    const vector<double> drap_bins = linspace(10, 0., 10.);

    bool isNLOPrompt(ConstGenParticlePtr p)
    {
      return true; // only works in HepMC2!!! TODO
/*
      bool ret = false;
      if (p == nullptr)
        return false;
      if (p->production_vertex()->barcode() == -3)
        return true;
      if (PID::isHadron(p->pdg_id()))
        return false;
      for (ConstGenParticlePtr ancestor : HepMCUtils::particles(p->production_vertex(), Relatives::ANCESTORS))
      {
        // particle should remain the same, only get some momentum reshuffles from shower
        if (ancestor->pdg_id() == p->pdg_id() && p->production_vertex()->particles_out_size() == 1)
        {
          // This should only be called once so we don't have to worry about overwriting ret
          ret = isNLOPrompt(ancestor);
        }
      }
      return ret;
*/
    }
    bool isNLOPrompt(const Rivet::Particle &p)
    {
      return isNLOPrompt(p.genParticle());
    }

    bool isLOPrompt(ConstGenParticlePtr p)
    {
      return false; // only works in HepMC2!!! TODO
/*
      bool ret = false;
      if (p == nullptr)
        return false;
      if (p->production_vertex()->barcode() == -3)
      {
        if (p->production_vertex()->particles_out_size() == 2)
          return true;
        else
          return false;
      }
      if (PID::isHadron(p->pdg_id()))
        return false;
      for (ConstGenParticlePtr ancestor : HepMCUtils::particles(p->production_vertex(), Relatives::ANCESTORS))
      {
        // particle should remain the same, only get some momentum reshuffles from shower
        if (ancestor->pdg_id() == p->pdg_id() && p->production_vertex()->particles_out_size() == 1)
        {
          // This should only be called once so we don't have to worry about overwriting ret
          ret = isLOPrompt(ancestor);
        }
      }
      return ret;
*/
    }
    bool isLOPrompt(const Rivet::Particle &p)
    {
      return isLOPrompt(p.genParticle());
    }

    struct AnaInfo
    {
      const Event *event;
      const Particle *ph;
      const Jets *jets;
      const Jet *jet;
      bool isolated = true;
    };
    struct MCInfo
    {
      // Monte Carlo info
      bool nloprompt = false, loprompt = false;
      bool gg = false, qg = false, qq = false;
      int id1 = -1, id2 = -1;
      double x1 = 0.0, x2 = 0.0, Q = 0.0;
    };

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init()
    {
      _mode = 1;
      if (getOption("NOTPOWHEG", "FALSE") == "TRUE")
        _mode = 0;

      FinalState af = FinalState();
      declare(af, "AllFinal");

      // Prompt here means direct, so no decays
      PromptFinalState focal_ph_fs = PromptFinalState(Cuts::abspid == PID::PHOTON && Cuts::etaIn(ph_eta_bins.at(0), ph_eta_bins.at(ph_eta_bins.size() - 1)) && Cuts::ptIn(ph_pt_bins.at(0), ph_pt_bins.at(ph_pt_bins.size() - 1)));
      declare(focal_ph_fs, "Photons");

      // Initialize the lone projection required
      FastJets fj = FastJets(FinalState(), FastJets::ANTIKT, JET_R);
      declare(fj, "AntiKtJets");

      // Book histograms
      dphi_plot<Stage::INIT>(nullptr, nullptr);
      compton_plot<Stage::INIT>(nullptr, nullptr);
      processes_plot<Stage::INIT>(nullptr, nullptr);
      pdf_plot<Stage::INIT>(nullptr, nullptr);
      dphi_deta_plot<Stage::INIT>(nullptr, nullptr);
    }

    /**
     *  Spliting stages inside of the 4-times loop is much nicer to look at and modify than having three times four loops.
     */

    template <Stage s>
    void compton_plot(const struct AnaInfo *ana, const struct MCInfo *mc)
    {
      std::string region = "";
      string n = "comptonrecoilyield";
      string nn = "DeltaPhiPhotonJetTMP";

      AUTO_CONDITION_DROP(ana->isolated)
      {
        AUTO_FOR_BIN(ana->ph->pt(), ph_pt_bins)
        {
          AUTO_FOR_BIN(ana->jet->pt(), j_pt_bins)
          {
            AUTO_BOOK(_h2, n, xg_bins, Q_bins);
            _AUTO_FOR_BIN_COND((((mc->id1 == 0 || mc->id1 == 21) && (AUTO_IN_BIN(mc->x1, xg_bins, xi))) ||
                                (((mc->id2 == 0 || mc->id2 == 21) && (AUTO_IN_BIN(mc->x2, xg_bins, xi))))),
                               xg_bins, xi, phjx)
            {
              _AUTO_FOR_BIN(mc->Q, Q_bins, qi, phjxq)
              {
                AUTO_BOOK(_h, nn, dphi_bins);
                AUTO_FILL(_h, nn, deltaPhi(*ana->ph, *ana->jet));
                AUTO_FINALIZE()
                {
                  // TODO divide by width/Normalize
                  // TODO handle uncertainties properly in integration
                  auto integ = _h[nn + region]->integralRange(_h[nn + region]->numBins() - 2, _h[nn + region]->numBins() - 1);
                  _h2[n + phjx]
                      ->fill(xg_bins[xi] / 2. + xg_bins[xi + 1] / 2., Q_bins[qi] / 2. + Q_bins[qi + 1] / 2., integ);
                }
              }
            }
            AUTO_BOOK_FILL("Q", Q_bins, mc->Q);
            AUTO_BOOK(_h, "xg", xg_bins);
            if (!isAnalyse<s>() || mc->id1 == 0 || mc->id1 == 21)
            {
              AUTO_FILL(_h, "xg", mc->x1);
            }
            if (!isAnalyse<s>() || mc->id2 == 0 || mc->id2 == 21)
            {
              AUTO_FILL(_h, "xg", mc->x2);
            }
          }
        }
      }
    }

    template <Stage s>
    void dphi_plot(const struct AnaInfo *ana, const struct MCInfo *mc)
    {
      string region = "";
      string n = "DeltaPhiPhotonJet";
      AUTO_CONDITION_TRUE(mc->nloprompt)
      {
        AUTO_CONDITION_DROP(ana->isolated)
        {
          AUTO_FOR_BIN(ana->ph->pt(), ph_pt_bins)
          {
            AUTO_FOR_BIN(ana->jet->pt(), j_pt_bins)
            {
              AUTO_BOOK(_h, n, dphi_bins);
              AUTO_FILL(_h, n, deltaPhi(*ana->ph, *ana->jet));
            }
          }
        }
      }
    }

    template <Stage s>
    void processes_plot(const struct AnaInfo *ana, const struct MCInfo *mc)
    {
      string region = "";
      string n = "pTPhoton";
      string nn = "pTPhoton_frac";
      const vector<double> proc_pt_bins = linspace(10, 5, 20);
      AUTO_CONDITION_TRUE(mc->nloprompt)
      {
        AUTO_BOOK_FILL(n, proc_pt_bins, ana->ph->pT());
        _AUTO_CONDITION_DROP(ana->isolated, b, save)
        {
          AUTO_CONDITION_TRUE(mc->qg)
          {
            AUTO_BOOK(_s2, nn, proc_pt_bins);
            AUTO_BOOK_FILL(n, proc_pt_bins, ana->ph->pT());
            AUTO_FINALIZE()
            divide(_h[n + region], _h[n + save], _s2[nn + region]);
          }
          AUTO_CONDITION_TRUE(mc->qq)
          {
            AUTO_BOOK(_s2, nn, proc_pt_bins);
            AUTO_BOOK_FILL(n, proc_pt_bins, ana->ph->pT());
            AUTO_FINALIZE()
            divide(_h[n + region], _h[n + save], _s2[nn + region]);
          }
          AUTO_CONDITION_TRUE(mc->gg)
          {
            AUTO_BOOK(_s2, nn, proc_pt_bins);
            AUTO_BOOK_FILL(n, proc_pt_bins, ana->ph->pT());
            AUTO_FINALIZE()
            divide(_h[n + region], _h[n + save], _s2[nn + region]);
          }
        }
      }
    }

    template <Stage s>
    void pdf_plot(const struct AnaInfo *ana, const struct MCInfo *mc)
    {
      string region = ""; // ONE can add different dependencies here

      const vector<double> pt_bins = linspace(10, 5, 20);

      AUTO_CONDITION(ana->isolated)
      {
        AUTO_CONDITION(mc->nloprompt)
        {
          AUTO_BOOK_FILL_2("pTPhoton-Q", pt_bins, Q_bins, ana->ph->pT(), mc->Q);
        }
      }

      AUTO_CONDITION_TRUE(ana->isolated)
      {
        AUTO_CONDITION_TRUE(mc->nloprompt)
        {
          AUTO_BOOK_FILL("Q", Q_bins, mc->Q);
          AUTO_BOOK(_h, "xg", xg_bins);
          AUTO_BOOK(_h2, "xg-Q", xg_bins, Q_bins);
          if (!isAnalyse<s>() || mc->id1 == 0 || mc->id1 == 21)
          {
            AUTO_FILL(_h, "xg", mc->x1);
            AUTO_FILL(_h2, "xg-Q", mc->x1, mc->Q);
          }
          if (!isAnalyse<s>() || mc->id2 == 0 || mc->id2 == 21)
          {
            AUTO_FILL(_h, "xg", mc->x2);
            AUTO_FILL(_h2, "xg-Q", mc->x2, mc->Q);
          }
        }
      }
    }

    template <Stage s>
    void dphi_deta_plot(const struct AnaInfo *ana, const struct MCInfo *mc)
    {
      string region = ""; // ONE can add different dependencies here

      const vector<double> dphi_bins = linspace(10, 0., M_PI);
      const vector<double> deta_bins = linspace(10, 0., 5.);
      const vector<double> dR_bins = linspace(10, 0., 5.);
      const vector<double> jet_bins = linspace(10, 0., 60.);
      AUTO_CONDITION_DROP(ana->isolated)
      {
        AUTO_CONDITION(mc->nloprompt)
        {
          AUTO_BOOK_FILL_2("dphi_deta_plot", dphi_bins, deta_bins, deltaPhi(*ana->ph, *ana->jet), deltaEta(*ana->ph, *ana->jet));
          AUTO_BOOK_FILL_2("dphi_drap_plot", dphi_bins, deta_bins, deltaPhi(*ana->ph, *ana->jet), deltaRap(*ana->ph, *ana->jet));
          AUTO_BOOK_FILL_2("dR_jet_plot", dR_bins, jet_bins, deltaR(*ana->ph, *ana->jet), ana->jet->Et());
        }
      }
    }

    /// Perform the per-event analysis
    void analyze(const Event &event)
    {
      AnaInfo ana;
      MCInfo mc;

      /*
       *  Get physical infos
       */
      Particles phs;
      Particles afs = apply<ParticleFinder>(event, "AllFinal").particlesByPt();
      phs = apply<ParticleFinder>(event, "Photons").particlesByPt();
      // we select the hardest photon
      if (phs.size() == 0)
        vetoEvent;
      ana.ph = &phs[0];
      const Particle ph = phs[0];
      // Isolation cut
      // sum of energy in cone
      double isoCone_et = 0.;
      for (const Particle &p : afs)
      {
        if (!p.isVisible())
          continue;
        /*
      // skip particles not found by Focal
      if (p.eta() < PHOTON_MIN_ETA - ISO_CONE_RADIUS || p.eta() > PHOTON_MAX_ETA + ISO_CONE_RADIUS)
        continue;
        */
        // skip self
        if (p.isSame(ph))
          continue;

        // MSG_WARNING("Rivet-particle: " <<deltaPhi(ph, p) << " "<< p.px() << " "<< p.py() << " "<< p.pz() << " ");
        //  we choose RAPDITIY over PSEUDORAPIDITY scheme here
        if (deltaR(ph, p, RAPIDITY) < ISO_CONE_RADIUS)
          isoCone_et += p.Et();
      }
      // jump to next event if hardest photon is not isolated
      if (isoCone_et >= ISO_CONE_ET)
      {
        ana.isolated = false;
      }

      Jets jets = apply<FastJets>(event, "AntiKtJets").jetsByPt(Cuts::etaIn(j_eta_bins.at(0), j_eta_bins.at(j_eta_bins.size() - 1)) && Cuts::ptIn(j_pt_bins.at(0), j_pt_bins.at(j_pt_bins.size() - 1)));
      ana.jets = &jets;
      if (jets.size() > 0)
      {
        ana.jet = &jets[0];

        // MSG_WARNING("Rivet-jet: " <<deltaPhi(ph, jets[0]) << " "<< jets[0].px() << " "<< jets[0].py() << " "<< jets[0].pz() << " ");
        // MSG_WARNING("Rivet-jet: " <<deltaPhi(ph, jets[0]) << " "<< jets[0].px() << " "<< jets[0].py() << " "<< jets[0].pz() << " " << ph.px() << " "<< ph.py() << " "<< ph.pz() << " ");
      }
      /*
       * Get Monte Carlo infos
       */
      if (event.genEvent()->pdf_info() != 0) // PDF data might not be available
      {
        PdfInfo pdfi = *(event.genEvent()->pdf_info());
        // TODO figure out how to reconstruct with out MC information
#ifdef RIVET_ENABLE_HEPMC_3
        mc.id1 = pdfi.pdf_id[0];
        mc.id2 = pdfi.pdf_id[1];
        mc.x1 = pdfi.x[0];
        mc.x2 = pdfi.x[1];
        mc.Q = pdfi.scale;
#else
        mc.id1 = pdfi.id1();
        mc.id2 = pdfi.id2();
        mc.x1 = pdfi.x1();
        mc.x2 = pdfi.x2();
        mc.Q = pdfi.scalePDF();
#endif
      }
      // Determine process from montecarlo ids
      mc.gg = ((mc.id1 == 0 || mc.id1 == 21) && (mc.id2 == 0 || mc.id2 == 21));
      mc.qq = (!(mc.id1 == 0 || mc.id1 == 21) && !(mc.id2 == 0 || mc.id2 == 21));
      mc.qg = ((mc.id1 == 0 || mc.id1 == 21) != (mc.id2 == 0 || mc.id2 == 21));
      // is the photon connected to the NLO tree process
      mc.nloprompt = isNLOPrompt(ph);
      mc.loprompt = isLOPrompt(ph);

      /*
       * Fill histos
       */

      Jet *first = nullptr;
      if (jets.size() > 0)
      {
        for (unsigned int i = 0; i < min(max_num_jets, jets.size()); i++)
        {
          if (deltaR(ph, jets[i], RAPIDITY) > ISO_CONE_RADIUS)
          {
            ana.jet = &jets[i];
            if (first == nullptr)
              first = &jets[i];
            dphi_plot<Stage::ANALYSE>(&ana, &mc);
            compton_plot<Stage::ANALYSE>(&ana, &mc);
            dphi_deta_plot<Stage::ANALYSE>(&ana, &mc);
          }
        }
        ana.jet = first;
      }
      processes_plot<Stage::ANALYSE>(&ana, &mc);
      pdf_plot<Stage::ANALYSE>(&ana, &mc);
    }

    /// Normalise histograms etc., after the run
    void finalize()
    {
      // We do not scale by cross section since the weights of the directphton POWHEG code are already scaled by the cross section.
      if (_mode == 1)
      {
        scale(_h, 1. / numEvents() / (picobarn));
        scale(_h2, 1. / numEvents() / (picobarn));
      }
      else
      {
        scale(_h, crossSectionPerEvent() / (picobarn));
        scale(_h2, crossSectionPerEvent() / (picobarn));
      }

      dphi_plot<Stage::FINALIZE>(nullptr, nullptr);
      compton_plot<Stage::FINALIZE>(nullptr, nullptr);
      processes_plot<Stage::FINALIZE>(nullptr, nullptr);
      pdf_plot<Stage::FINALIZE>(nullptr, nullptr);
      dphi_deta_plot<Stage::FINALIZE>(nullptr, nullptr);
    }

    //@}

    /// @name Histograms
    //@{
    map<string, Histo1DPtr> _h;
    map<string, Histo2DPtr> _h2;
    map<string, Scatter2DPtr> _s2;
    map<string, Scatter3DPtr> _s3;
    // map<string, Scatter2DPtr> _s;
    //@}

    // mode sets if cross section is already included in the weight
    size_t _mode;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ALICE_2022_FOCAL);
}
